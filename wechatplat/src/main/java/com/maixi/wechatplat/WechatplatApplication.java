package com.maixi.wechatplat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class WechatplatApplication {

    public static void main(String[] args) {
        SpringApplication.run(WechatplatApplication.class, args);
    }

}
