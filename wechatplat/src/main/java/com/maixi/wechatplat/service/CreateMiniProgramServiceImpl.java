package com.maixi.wechatplat.service;

import com.maixi.wechatplat.entity.CreateMiniProgramReq;
import com.maixi.wechatplat.mapper.CreateMiniProgramReqMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("createMiniProgramService")
public class CreateMiniProgramServiceImpl implements CreateMiniProgramService {
    @Autowired
    private CreateMiniProgramReqMapper createMiniProgramReqMapper;

    //数据库添加创建小程序任务
    public int addCreateMiniProgramReq(CreateMiniProgramReq createMiniProgramReq) {
        int ret = createMiniProgramReqMapper.addCreateMiniProgramReq(createMiniProgramReq);
        return ret;
    }
    //查询所有创建任务
    public List<CreateMiniProgramReq>queryCreateMiniProgramList() {
        return createMiniProgramReqMapper.queryCreateMiniProgramList();
    }
    public int updateCreateMiniPogramReqStatus(CreateMiniProgramReq createMiniProgramReq) {
        return createMiniProgramReqMapper.updateCreateMiniPogramReqStatus(createMiniProgramReq);
    }
}
