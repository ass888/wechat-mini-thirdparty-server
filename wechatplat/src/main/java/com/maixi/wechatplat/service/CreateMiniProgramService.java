package com.maixi.wechatplat.service;

import com.maixi.wechatplat.entity.CreateMiniProgramReq;

import java.util.List;

public interface CreateMiniProgramService {
    public int addCreateMiniProgramReq(CreateMiniProgramReq createMiniProgramReq);
    public List<CreateMiniProgramReq> queryCreateMiniProgramList();
    public int updateCreateMiniPogramReqStatus(CreateMiniProgramReq createMiniProgramReq);
}
