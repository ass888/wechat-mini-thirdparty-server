package com.maixi.wechatplat.service;

import com.maixi.wechatplat.entity.AccessTokenReq;
import com.maixi.wechatplat.utils.GloabCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("WechatThirdPartyService")
public class WechatThirdPartyServiceImpl implements WechatThirdPartyService {
    @Autowired
    private WechatThirdPartyCompentNetService wechatThirdPartyCompentNetService;

    public String queryComponentAccessToken(String appid,String appSecret) {
        System.out.println("query access token");
        //先去缓存找 ，找不到或者过期 则调用微信接口获取并存到缓存
        Map<String,Object>map = GloabCache.getCompentTokenInfo(appid);
        if (map != null) {//缓存中有token
            System.out.println("token map not null");
            String component_access_token = map.get("component_access_token").toString();
            if (component_access_token != null && component_access_token.length() > 0) {
                String expireTime = map.get("expireTime").toString();
                long expireTimeSecs = Long.parseLong(expireTime);
                long currentTimeSecs = System.currentTimeMillis() * 1000;
                if (currentTimeSecs <= expireTimeSecs) {//未过期
                    return component_access_token;
                }
            }
        }
        System.out.println("request token appid:"+ appid);
        //过期或者没有token，调用接口查询
        //先获取票据
        Map<String,Object> ticketInfoMap = GloabCache.getTicketInfo(appid);
        if (ticketInfoMap != null) {
            System.out.println("ticketmap："+ticketInfoMap);

            String ticket = ticketInfoMap.get("component_verify_ticket").toString();
            if (ticket != null && ticket.length() > 0) {
                String ticket_expireTime = ticketInfoMap.get("expireTime").toString();
                long ticket_expireTimeSec = Long.parseLong(ticket_expireTime);
                long timeSec = System.currentTimeMillis() / 1000;
                System.out.println("currentTime:"+timeSec);
                System.out.println("exoireTimeSec:" + ticket_expireTimeSec);
                if (timeSec <= ticket_expireTimeSec) {//ticket未过期
                    AccessTokenReq req = new AccessTokenReq();
                    req.setComponent_appid(appid);
                    req.setComponent_appsecret(appSecret);
                    req.setComponent_verify_ticket(ticket);
                    //获取token
                    Map<String, Object>tokenResponsMap =  wechatThirdPartyCompentNetService.getComponentAccessToken(req);
                    if (tokenResponsMap != null) {
                        //将返回的token打包缓存
                        String token = tokenResponsMap.get("component_access_token").toString();
                        String expireDurTime = tokenResponsMap.get("expires_in").toString();
                        long expireDurTimeSec = Long.parseLong(expireDurTime);
                        long curtimeSec = System.currentTimeMillis() * 1000;
                        long expireTimeSec = curtimeSec + expireDurTimeSec;
                        Map<String, Object>tokenMap = new HashMap<>();
                        tokenMap.put("appid", appid);
                        tokenMap.put("component_access_token", token);
                        tokenMap.put("expireTime",expireTimeSec);
                        GloabCache.setCompentTokenInfo(appid,tokenMap);

                        System.out.println("finish get token");
                        return token;
                    }
                    else {
                        System.out.println("null tokenResponsMap");
                    }
                }
                else {
                    System.out.println("ticket expire");
                }
            }
            else {
                System.out.println("ticket null");
            }
        }
        else {
            System.out.println("ticketInfomap null");
        }
        return "";
    }
}
