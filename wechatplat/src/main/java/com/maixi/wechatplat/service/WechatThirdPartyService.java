package com.maixi.wechatplat.service;

import org.springframework.stereotype.Service;

public interface WechatThirdPartyService {
    public String queryComponentAccessToken(String appid, String appSecret);
}
