package com.maixi.wechatplat.service;

import com.maixi.wechatplat.entity.AccessTokenReq;
import com.maixi.wechatplat.entity.CreateMiniProgramReq;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(url = "https://api.weixin.qq.com/cgi-bin/component",name = "wechatThirdPartyCompentNetService")
public interface WechatThirdPartyCompentNetService {
    //获取token
    @RequestMapping(value = "/api_component_token", method = RequestMethod.POST)
    public Map<String, Object>getComponentAccessToken(@RequestBody AccessTokenReq requestBody);

    //创建小程序
    @RequestMapping(value = "/fastregisterweapp", method = RequestMethod.POST)
    public Map<String, Object>createMiniProgarm(@RequestParam(value = "action") String action, @RequestParam(value = "component_access_token")String token, @RequestBody Map<String,Object> reqBody);
}
