package com.maixi.wechatplat.controller;

import com.maixi.wechatplat.entity.CreateMiniProgramReq;
import com.maixi.wechatplat.service.CreateMiniProgramService;
import com.maixi.wechatplat.service.WechatThirdPartyCompentNetService;
import com.maixi.wechatplat.service.WechatThirdPartyService;
import com.maixi.wechatplat.utils.AesException;
import com.maixi.wechatplat.utils.GloabCache;
import com.maixi.wechatplat.utils.StringUtils;
import com.maixi.wechatplat.utils.WXBizMsgCrypt;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class WechatThirdPartyAuthContoller {
    @Autowired
    private WechatThirdPartyService wechatThirdPartyService;

    @Autowired
    private WechatThirdPartyCompentNetService wechatThirdPartyCompentNetService;

    @Autowired
    private CreateMiniProgramService createMiniProgramService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET,produces = "application/json;charset=UTF-8")
    public String hello() {
        CreateMiniProgramReq req = new CreateMiniProgramReq();
        req.setThirdpartAppid("wxbc523f38a4caf22f");
        req.setLegalPersonWechatNum("wx2");
        req.setMerchantCode("23");
        req.setStatus(1);
        req.setReason("face fail");
        req.setAuthCode("888");
        req.setIsAuthed(1);

        int ret = createMiniProgramService.updateCreateMiniPogramReqStatus(req);
        return "hello";
    }
    @RequestMapping(value = "/testXml", method = RequestMethod.POST)
    public Map<String, Object>  testXml(@RequestBody String requestBody) throws DocumentException {
        //System.out.println("test xml req"+ requestBody);
        Map<String, Object> map = StringUtils.XmlStringToMap(requestBody);
        System.out.println("xml map"+ map);
        return map;
    }

    @RequestMapping(value = "/authNotify", method = RequestMethod.POST)
    public String authNotify(HttpServletRequest request, @RequestBody String requestBody) throws DocumentException, AesException {
        System.out.println("auth notify");

        //String signature=request.getParameter("signature");
        String nonce = request.getParameter("nonce");
        String timestamp = request.getParameter("timestamp");
        String msg_signature = request.getParameter("msg_signature");
        String token = "c8jasddddxo191ashdhd881s";
        String encodingAesKey = "b2va93qz0c1kkkierya65sgss4acmoqflas6d8a1ndx";
        String thirdParty_appid = "wxbc523f38a4caf22f";
        //解密消息体
        WXBizMsgCrypt pc = new WXBizMsgCrypt(token, encodingAesKey, thirdParty_appid);
        String msg = pc.decryptMsg(msg_signature,timestamp,nonce,requestBody);
        System.out.println("decrpt msg" + msg);
        Map msgMap = StringUtils.XmlStringToMap(msg);
        //String appid = msgMap.get("AppId").toString();
        String createTime = msgMap.get("CreateTime").toString();
        String infoType = msgMap.get("InfoType").toString();
        if (infoType.equals("component_verify_ticket")) {//ticket
            String componentVerifyTicket = msgMap.get("ComponentVerifyTicket").toString();
            Map<String,Object> ticketMap = new HashMap<>();
            ticketMap.put("component_verify_ticket",componentVerifyTicket);
            ticketMap.put("component_appid", thirdParty_appid);
            String creatTime = msgMap.get("CreateTime").toString();
            long createTimeSec = Long.parseLong(createTime);
            long expireTimeSec = createTimeSec + 12 * 60 * 60;//12小时有效期
            ticketMap.put("expireTime",expireTimeSec);
            //缓存票据信息 用于获取access_token
            GloabCache.setTicketInfo(thirdParty_appid, ticketMap);
            System.out.println("save ticket appid:"+ thirdParty_appid);
            System.out.println("ticket map"+ ticketMap);
        }
        else if (infoType.equals("notify_third_fasteregister")) {//创建小程序审核结果事件
            //String
            System.out.println("notify_third_fasteregister" + msgMap);
            String statusMsg = msgMap.get("msg").toString();
            String thirdPartyAppid = msgMap.get("AppId").toString();
            int status = Integer.parseInt(msgMap.get("status").toString());
            String auth_code = msgMap.get("auth_code").toString();
            Object infoObject = msgMap.get("info");
            Map<String,Object>infoMap = (Map<String,Object>)infoObject;
            String merchantcode = infoMap.get("code").toString();
            String wechatNum = infoMap.get("legal_persona_wechat").toString();
            //更新任务状态
            CreateMiniProgramReq req = new CreateMiniProgramReq();
            req.setThirdpartAppid(thirdPartyAppid);
            req.setLegalPersonWechatNum(wechatNum);
            req.setMerchantCode(merchantcode);
            req.setStatus(status);
            req.setReason(statusMsg);
            req.setAuthCode(auth_code);
            if (status == 0) {
                req.setIsAuthed(1);
            }
            System.out.println("fasteregister statusMsg:"+ statusMsg + "-status:"+status + "-auth_code:"+ auth_code + "-merhantCode:"+ merchantcode + "-wchatNum:"+ wechatNum);
            int ret = createMiniProgramService.updateCreateMiniPogramReqStatus(req);
            System.out.println("fasteregister update:"+ ret);
        }
        else {
            System.out.println("unknow ticket type");
        }
        return "success";
    }

    //快速创建小程序
    @RequestMapping(value = "createMiniProgram", method = RequestMethod.POST)
    public Map<String,Object> createMiniProgram(@RequestParam(value = "appid")String appid, @RequestParam(value = "appSecret")String appSecrect, @RequestBody CreateMiniProgramReq requestBody) {
        System.out.println("createMiniProgram request");
        Map<String, Object>map = new HashMap<>(3);

        //先根据第三方平台appid获取component_access_token
        String accessToken = wechatThirdPartyService.queryComponentAccessToken(appid, appSecrect);
        String errStr = "未知原因";
        if (accessToken != null && accessToken.length() > 0) {
            //调用微信接口创建
            Map<String, Object>responMap = wechatThirdPartyCompentNetService.createMiniProgarm("create",accessToken,requestBody.requestMap());
            if (responMap != null) {
                Object obj = responMap.get("errcode");
                if (obj != null) {
                    int code = Integer.parseInt(obj.toString());
                    if (requestBody.getId() <= 0) {//新创建
                        requestBody.setThirdpartAppid(appid);
                        long currentTimeSecs = System.currentTimeMillis() * 1000;
                        requestBody.setCreateTime(currentTimeSecs);
                        int ret = createMiniProgramService.addCreateMiniProgramReq(requestBody);
                        System.out.println("create id" + ret);
                    }
                }
                map.put("result", responMap);
                map.put("success", 1);
                return map;
            }
        }
        else  {
            errStr = "token未找到";
        }
        map.put("success", 0);
        map.put("err", errStr);
        return map;
    }
    //获取所有创建小程序任务
    @RequestMapping(value = "/getCreateMiniProgramList", method = RequestMethod.GET,produces = "application/json;charset=UTF-8")
    public Map<String, Object> getCreateMiniProgramList() {
        Map<String, Object> map = new HashMap<>();
        List<CreateMiniProgramReq> list = createMiniProgramService.queryCreateMiniProgramList();
        map.put("success",1);
        if (list != null && list.size() > 0) {
            map.put("list", list);
        }
        return map;
    }

}
