package com.maixi.wechatplat;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.boot.SpringApplication;


@SpringBootApplication

public class SpringbootApplcation extends SpringBootServletInitializer {

    public static void main(String[] args) {

        SpringApplication.run(SpringbootApplcation.class,args) ;

    }



    @Override

    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

// 注意这里要指向原先用main方法执行的Application启动类

        return builder.sources(SpringbootApplcation.class);

    }

}
