package com.maixi.wechatplat.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CreateMiniProgramReq implements Serializable {
    private int id;
    private String merchantName;//企业名
    private String merchantCode;//企业代码
    private int merchantCodeType;//企业代码类型（1：统一社会信用代码， 2：组织机构代码，3：营业执照注册号）
    private String legalPersonWechatNum;//法人微信号
    private String legalPersonName;//法人姓名
    private String merchantPhone;//第三方联系电话
    private int status;//审核状态，其中0为审核成功。isAuthed为真时有效
    private String reason;//拒绝原因
    private String authCode;//授权码
    private int isAuthed;//是否已授权 用于后面在第三方平台取消授权
    private String thirdpartAppid;//第三方平台appid
    private long createTime;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public int getMerchantCodeType() {
        return merchantCodeType;
    }

    public void setMerchantCodeType(int merchantCodeType) {
        this.merchantCodeType = merchantCodeType;
    }

    public String getLegalPersonName() {
        return legalPersonName;
    }

    public void setLegalPersonName(String legalPersonName) {
        this.legalPersonName = legalPersonName;
    }

    public String getLegalPersonWechatNum() {
        return legalPersonWechatNum;
    }

    public void setLegalPersonWechatNum(String legalPersonWechatNum) {
        this.legalPersonWechatNum = legalPersonWechatNum;
    }

    public String getMerchantPhone() {
        return merchantPhone;
    }

    public void setMerchantPhone(String merchantPhone) {
        this.merchantPhone = merchantPhone;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public void setIsAuthed(int isAuthed) {
        this.isAuthed = isAuthed;
    }

    public int getIsAuthed() {
        return isAuthed;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getCreateTime() {
        return createTime;
    }

    public String getThirdpartAppid() {
        return thirdpartAppid;
    }

    public void setThirdpartAppid(String thirdpartAppid) {
        this.thirdpartAppid = thirdpartAppid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Map<String, Object>requestMap() {
        Map<String,Object>map = new HashMap<>();
        map.put("name", merchantName);
        map.put("code",merchantCode);
        map.put("code_type",merchantCodeType);
        map.put("legal_persona_wechat",legalPersonWechatNum);
        map.put("legal_persona_name",legalPersonName);
        map.put("component_phone",merchantPhone);

        return map;
    }
}

