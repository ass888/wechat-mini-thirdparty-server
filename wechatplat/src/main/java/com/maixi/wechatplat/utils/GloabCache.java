package com.maixi.wechatplat.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class GloabCache {
    private static Map<String, Map>wechatCompentTokenMapCache = new HashMap<>();
    private static Map<String, Map>wechatTickeMapCache = new HashMap<>();

    public static void setCompentTokenInfo(String key, Map values) {
        wechatCompentTokenMapCache.put(key, values);
    }

    public static Map getCompentTokenInfo(Object key) {
        return wechatCompentTokenMapCache.get(key);
    }

    public static void  setTicketInfo(String key, Map values) {
        wechatTickeMapCache.put(key, values);
    }
    public static Map getTicketInfo(String key) {
        return wechatTickeMapCache.get(key);
    }
}
