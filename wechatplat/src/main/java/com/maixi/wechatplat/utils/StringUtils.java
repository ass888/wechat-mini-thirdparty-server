package com.maixi.wechatplat.utils;

import com.maixi.wechatplat.entity.CreateMiniProgramReq;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringUtils {
    public  static  Map<String,Object>XmlStringToMap(String xmlString) throws DocumentException {
        // 将解析结果存储在Map中
        Map map = new HashMap();
        InputStream in=new ByteArrayInputStream(xmlString.getBytes());

// 读取输入流
        SAXReader reader = new SAXReader();
        Document document = reader.read(in);
// 得到xml根元素
        Element root = document.getRootElement();
// 得到根元素的所有子节点
        element2Map(map, root);

        Map<String, Object> resultMap = (Map<String, Object>)map.get("xml");
        if (resultMap == null) {
            resultMap = map;
        }
        return resultMap;
    }

    private static void element2Map(Map<String, Object> map, Element rootElement) {
        // 获得当前节点的子节点
        List<Element> childElements = rootElement.elements();
        if (childElements.size() > 0) {
            Map<String, Object> tempMap = new HashMap<>();
            for (Element e : childElements) {
                element2Map(tempMap, e);
                map.put(rootElement.getName(), tempMap);
            }
        } else {
            map.put(rootElement.getName(), rootElement.getText());
        }
    }
}
