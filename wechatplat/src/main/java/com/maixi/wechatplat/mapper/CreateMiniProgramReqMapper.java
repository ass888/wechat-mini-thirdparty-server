package com.maixi.wechatplat.mapper;

import com.maixi.wechatplat.entity.CreateMiniProgramReq;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CreateMiniProgramReqMapper {
    public int addCreateMiniProgramReq(CreateMiniProgramReq req);
    public List<CreateMiniProgramReq>queryCreateMiniProgramList();
    public int updateCreateMiniPogramReqStatus(CreateMiniProgramReq createMiniProgramReq);
}
